package com.twuc.webApp.domain.mazeRender;

import java.awt.*;

/**
 * 使用指定的方式渲染一个迷宫节点。
 */
public interface CellRender {

    /**
     * 使用指定的方式渲染一个迷宫的节点。
     *
     * @param context 渲染目标。
     * @param cellArea 迷宫的节点所在的像素位置。
     * @param cell 迷宫渲染节点的信息。注意。{@link RenderCell} 和 {@link com.twuc.webApp.domain.mazeGenerator.GridCell}
     *             不同，前者包含更加丰富的用于渲染的信息。而后者仅仅包含路径信息。
     */
    void render(
        Graphics context,
        Rectangle cellArea,
        RenderCell cell);
}
